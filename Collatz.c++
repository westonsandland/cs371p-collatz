// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair

#include "Collatz.h"

using namespace std;

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s)
{
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ---------------
// optimize_inputs
// ---------------

void optimize_inputs (int& st, int& sp) //optimizes inputs by halving the top value in certain cases
{
    assert(sp>=st);
    int possibleSt = sp/2+1;
    if(possibleSt > st)
    {
        st = possibleSt;
    }
    assert(sp>=st);
}

// ------------
// collatz_eval
// ------------

int memoArray[1000000] = {0}; //array for memoization

int collatz_recursive (long c)
{
    //printf("%d\n",c);
    assert(c>=1);
    if(c == 1)
        return 1;
    if(c >= 1000000) //dont cache if its over the limit
    {
        if(c%2 == 0)
            return collatz_recursive(c/2) + 1; //even case
        return collatz_recursive((c*3+1)/2) + 2; //odd case
    }
    if(!memoArray[(int)c]) //if it's not in the cache, put it in there and make the next recursive call
    {
        if(c%2 == 0)
            memoArray[(int)c] = collatz_recursive(c/2) + 1; //even case
        else
            memoArray[(int)c] = collatz_recursive(c*3+1) + 1; //odd case
    }
    return memoArray[(int)c]; //returns whatever is saved for that value
}

int collatz_eval (int i, int j)
{
    assert(i>0);
    assert(j>0);
    int start;
    int stop;
    long k; //iterative integer
    int mcl = 0; //max cycle length
    int cl = 0; //current cycle length
    memoArray[1] = 1;
    if(i<j)
    {
        start = i; //makes sure start/stop come before/after
        stop = j;
    }
    else
    {
        start = j;
        stop = i;
    }
    optimize_inputs(start, stop); //does the divide by 2 plus 1 optimization
    for(k = start; k <= stop; k++)
    {
        cl = 0; //resets cycle length
        cl = collatz_recursive(k);
        if(cl > mcl)
            mcl = cl;
    }
    assert(start<=stop);
    assert(mcl>=cl);
    assert(mcl>=0);
    return mcl;
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v)
{
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w)
{
    string s;
    while (getline(r, s))
    {
        const pair<int, int> p = collatz_read(s);
        const int            i = p.first;
        const int            j = p.second;
        const int            v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}
