// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <sstream>  // istringtstream, ostringstream
#include <string>   // string
#include <utility>  // pair

#include "gtest/gtest.h"

#include "Collatz.h"

using namespace std;

// -----------
// TestCollatz
// -----------

// ----
// read
// ----

TEST(CollatzFixture, read)
{
    string s("1 10\n");
    const pair<int, int> p = collatz_read(s);
    ASSERT_EQ(p.first,   1);
    ASSERT_EQ(p.second, 10);
}

// --------
// optimize
// --------

TEST(CollatzFixture, optimize_1)
{
    int begin = 10;
    int end = 1000;
    optimize_inputs(begin, end);
    ASSERT_EQ(begin, 501);
}

TEST(CollatzFixture, optimize_2)
{
    int begin = 7;
    int end = 10;
    optimize_inputs(begin, end);
    ASSERT_EQ(begin, 7);
}

TEST(CollatzFixture, optimize_3)
{
    int begin = 1000;
    int end = 1001;
    optimize_inputs(begin, end);
    ASSERT_EQ(begin, 1000);
}

TEST(CollatzFixture, optimize_4)
{
    int begin = 10;
    int end = 1001;
    optimize_inputs(begin, end);
    ASSERT_EQ(begin, 501);
}

TEST(CollatzFixture, optimize_5)
{
    int begin = 1001;
    int end = 1330;
    optimize_inputs(begin, end);
    ASSERT_EQ(begin, 1001);
}

TEST(CollatzFixture, optimize_6)
{
    int begin = 10;
    int end = 10;
    optimize_inputs(begin, end);
    ASSERT_EQ(begin, 10);
}
// ----
// eval
// ----

TEST(CollatzFixture, eval_1)
{
    const int v = collatz_eval(1, 1);
    ASSERT_EQ(v, 1);
}

TEST(CollatzFixture, eval_2)
{
    const int v = collatz_eval(1, 2);
    ASSERT_EQ(v, 2);
}

TEST(CollatzFixture, eval_3)
{
    const int v = collatz_eval(2, 3);
    ASSERT_EQ(v, 8);
}

TEST(CollatzFixture, eval_4)
{
    const int v = collatz_eval(16, 15);
    ASSERT_EQ(v, 18);
}

TEST(CollatzFixture, eval_5)
{
    const int v = collatz_eval(71558, 644455);
    ASSERT_EQ(v, 509);
}

// -----
// print
// -----

TEST(CollatzFixture, print)
{
    ostringstream w;
    collatz_print(w, 1, 10, 20);
    ASSERT_EQ(w.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve)
{
    istringstream r("1 1\n1 2\n2 3\n16 15\n");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("1 1 1\n1 2 2\n2 3 8\n16 15 18\n", w.str());
}
